### Barclays' Openmidns Hackathon - Text helper ###

*This app was created during this 24 hour hackathon. We had a couple of options to choose from and we decided to work with Unicef to develop a smart way of helping people with technology in the poorer parts of the world.
Our idea was simple - get Google Now/Apple's Siri/Microsoft Cortana functionality, but on the normal phones, who don't even have Internet connection.
*

**Languages/Libraries used:
**

* Javascript (NodeJS, Twillio API, Wolfram Alpha API)

**How it works:**

We decided to simulate the functionality using text messages. When a question is sent to our number (Twillio), the request is sent to our server, where we invoke a request to Wolfram Answer to get the answer, then we sent the answer to our phone, which then sends the answer as a text message to the person who originally asked the questions.



**Screenshots:
**

![Screenshot1.png](https://bitbucket.org/repo/GABKz9/images/688185327-Screenshot1.png)
![Screenshot.png](https://bitbucket.org/repo/GABKz9/images/2789326299-Screenshot.png)